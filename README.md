# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

This is a basic project, but it was done to:
* practicing fundamental concepts
* create my fist Vite project
* Deploy to Gitlab pages

### Links

- [Live Site URL](https://frontendchallenges.gitlab.io/product-preview/)

## My process

1. Think of the design by splitting the layout in Components
2. Create tokens.scss with the constant values for colors, fonts, and sizes
3. Set a backgroud color for body
4. Create a card center vertically and horizontally
5. Mobile first: inside the card set a flex layout image first, then card-body. This way when desktop, the image will be left and body right.
6. Inside the body, we have a h2, h1, a p.description, original-price, new-price and finally a cta (button)
7. Deploy to gitlab pages

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite

### Lessons

How to set image src using CSS. This is useful when the image must change according to the screen.
Also the display: block was important to avoid a little gap created after the image. According to stackoverflow the gap is due to a small space down the letters for inferior part of some letters like p,q,y.

```css
&--image {
    content: url(../assets/image-product-mobile.jpg);
    display: block;
```

How to calculate the with of the image to use all the space avaiable, except for the margin.
```css
width: calc(100vw - 2em);
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.